package org.amicofragile.groovytrial;

import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.amicofragile.groovytrial.greetings.MultiGreeter;
import org.junit.Test;

public class JavaTestForGroovyClassTest {
	@Test
	public void runsGrrovyClassFromJavaCode() {
		MultiGreeter mg = new MultiGreeter();
		List<String> to = Arrays.asList(new String[] {"Goofy", "Donald Duck"});
		List<String> greetings = (List<String>) mg.greet(to);
		Assert.assertEquals(Arrays.asList(new String[] {"Hello, Goofy!", "Hello, Donald Duck!"}).toString(), greetings.toString());
	}
}
