package org.amicofragile.groovytrial.greetings;

import static org.junit.Assert.*;

import org.amicofragile.groovytrial.greetings.Greeter;
import org.junit.Before
import org.junit.Test

class GreeterTest {
	def Greeter greeter
	
	@Before
	def void initGreeter() {
		greeter = new Greeter()
	}
	
	@Test
	public void givenNoMessageNorTargetThenSaysHelloWorld() {
		assert "Hello, World!" == greeter.greet()
	}
	
	@Test
	public void givenTargetThenUsesItInGreeting() {
		assert "Hello, Goofy!" == greeter.greet("Goofy")
	}
	
	@Test
	def void givenCustomMessageThenUsesItInGreetings() {
		assert "Hy, Goofy!" == new Greeter("Hy").greet("Goofy")
	}
}
