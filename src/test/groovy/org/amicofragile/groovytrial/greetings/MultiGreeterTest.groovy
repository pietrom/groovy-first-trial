package org.amicofragile.groovytrial.greetings

import org.junit.Test
import org.junit.Assert

class MultiGreeterTest {
	@Test
	def void givenTargetListThenBuildsGreetingList () {
		def greetings = new MultiGreeter().greet(["Goofy", "Donald Duck"])
		assert ["Hello, Goofy!", "Hello, Donald Duck!"] == greetings
	}
}
