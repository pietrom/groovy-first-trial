package org.amicofragile.groovytrial.operators

import org.junit.Before
import org.junit.Test

class ComplexTest {
	def c1, c2, product

	@Before
	def void initFixtures() {
		c1 = new Complex(1, 2)
		c2 = new Complex(10, 15)
		product = new Complex(-3, -6)
	}

	@Test
	def void sumTwoComplex() {
		def sum = c1 + c2
		assert new Complex(11, 17) == sum
	}

	@Test
	def void multiplyComplexAndNumber() {
		assert product == c1 * -3
	}

	@Test
	def void multiplyNumberAndComplex() {
		use(ComplexCategory.class) {
			assert product == -3 * c1
		}
	}
}
