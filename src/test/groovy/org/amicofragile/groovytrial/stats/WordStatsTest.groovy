package org.amicofragile.groovytrial.stats

import org.junit.Test

class WordStatsTest {
	@Test
	def void readAndBuildStats() {
		def input = getClass().getResourceAsStream("blowin.txt")
		def words = [:]
		input.eachLine { line ->
			def tokens = line.tokenize(" ,.?!")
			tokens.each { token ->
				token = token.toLowerCase()
				words[token] = words.get(token, 0) + 1
			}
		}
		assert words['answer'] == 6
		assert words['the'] == 16
	}
}
