package org.amicofragile.groovytrial.markupbuilder

import org.junit.Test
import org.custommonkey.xmlunit.XMLUnit
import org.custommonkey.xmlunit.XMLAssert

class SimpleMarkupBuilderTest {
	@Test
	def void simpleMarkupBuilder() {
		def writer = new StringWriter()
		def builder = new groovy.xml.MarkupBuilder(writer)
		builder.setDoubleQuotes(true)
		def friendnames = ["Julie", "Joey", "Hannah"]

		builder.person() {
			name(first:"Megan", last:"Smith") {
				age("32")
				gender("female")
			}
			friends() {
				for (e in friendnames) {
					friend(e)
				}
			}
		}
		
		def expected = """
<person>
		<name first="Megan" last="Smith">
			<age>32</age>
			<gender>female</gender>
		</name>
		<friends>
			<friend>Julie</friend>
			<friend>Joey</friend>
			<friend>Hannah</friend>
		</friends>
</person>
"""
		def markup = writer.toString()
		XMLUnit.setIgnoreWhitespace(true)
		XMLAssert.assertXMLEqual(expected, markup)
	}
}
