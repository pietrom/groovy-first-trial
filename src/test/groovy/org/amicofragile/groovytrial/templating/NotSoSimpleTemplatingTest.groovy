package org.amicofragile.groovytrial.templating

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit
import org.junit.Test;

class NotSoSimpleTemplatingTest {
	@Test
	def void rendersPageDeclarationAndDoctype() {
		def jspProcessor = new JSPProcessor()
		def expected = """
		<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
		"""
		def heading = jspProcessor.heading()
	}
	
	@Test
	def void aBasicMDAGenerator() {
		def form = new Form("theForm")
		form.addField(new TextField("aText"))
		form.addField(new NumberField("aNumber"))
		
		def jspProcessor = new JSPProcessor()
		
		def page = jspProcessor.renderHtml(form)
		
		def expected = """\
<html>
		<head>
			<title>THEFORM</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		</head>
		<body>
			<form id="theForm">
				<input type="text" data-lit-type="text" />
				<input type="text" data-lit-type="number" />
			</form>
		</body>
</html>
"""
		XMLUnit.setIgnoreWhitespace(true)
		XMLAssert.assertXMLEqual(expected, page)
	}
	
	@Test
	def void applyToConcatenatesHeadingAndHtml() {
		def jspProcessor = new JSPProcessor()
		jspProcessor.metaClass.heading = {
			"HEADING"
		}
		jspProcessor.metaClass.renderHtml = {
			"HTML"
		}
		def expected = """<!-- Generated JSP Page -->
		HEADING
		HTML
		"""
		def page = jspProcessor.applyTo( new Object())
		assert expected == page
	}
}
