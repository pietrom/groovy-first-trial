package org.amicofragile.groovytrial.templating

import org.junit.Test

class SimpleTemplatingTest {
	def class Var {
		def showContent() {
			"Method content here!"
		}
	}
	
	@Test
	def void inStringTemplateProcessing() {
		def theVar = "World"
		assert "Hello, World!" == "Hello, $theVar!"
	}

	@Test
	def void inStringMethodInvocation() {
		def theVar = new Var()
		assert "InString invocation returns |Method content here!|" == "InString invocation returns |${theVar.showContent()}|"
	}
}
