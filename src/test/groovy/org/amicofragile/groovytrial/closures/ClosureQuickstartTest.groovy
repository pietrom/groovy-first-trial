package org.amicofragile.groovytrial.closures

import org.junit.Test

class ClosureQuickstartTest {
	@Test
	def void defineAndCall() {
		def adder = {x, y ->
			x + y
		}

		assert 10 == adder(2, 8)
		assert 10 == adder.call(1, 9)
	}

	@Test
	def void defaultValuesForParams() {
		def adderWithDefault = {x, y = 10 ->
			x + y
		}

		assert 10 == adderWithDefault(2, 8)
		assert 10 == adderWithDefault(0)
		assert 19 == adderWithDefault.call(9)
	}

	@Test
	def void defaultValuesAsParamDuplicate() {
		def adderWithDefault = {x, y = x ->
			x + y
		}

		assert 10 == adderWithDefault(2, 8)
		assert 10 == adderWithDefault(5)
	}

	class AClass {
		def base
		AClass(base) {
			this.base = base
		}
		def foo(x, y) {
			base + x + y
		}
	}

	@Test
	def void closureFromMethod() {
		def anInstance = new AClass('XX')
		def bar = anInstance.&foo
		def result = bar('AA', 'BB')
		assert 'XXAABB' == result
	}
}
