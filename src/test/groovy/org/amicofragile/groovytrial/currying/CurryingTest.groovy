package org.amicofragile.groovytrial.currying

import org.junit.Test

class CurryingTest {
	@Test
	def void simpleCurrying() {
		def aClosure = { x, y ->
			x + y
		}

		def partial = aClosure.curry(10)

		assert 15 == partial(5)
		assert 100 == partial(90)
	}
}
