package org.amicofragile.groovytrial

import org.junit.Before
import org.junit.Test

class NumberTest {
	def String accumulator
	def appendCurrentToAccumulator = {current ->
		accumulator += " $current"
	}
	
	@Before
	def void initAccumulator() {
		accumulator = ''
	}
	
	@Test
	def void tryUpto() {
		5.upto(15, appendCurrentToAccumulator)
		assert ' 5 6 7 8 9 10 11 12 13 14 15' == accumulator
	}
	
	@Test
	def void tryDownto() {
		10.downto(7, appendCurrentToAccumulator)
		assert ' 10 9 8 7' == accumulator
	}
	
	@Test
	def void tryStep() {
		1.step(10, 2, appendCurrentToAccumulator)
		assert ' 1 3 5 7 9' == accumulator
	}
}
