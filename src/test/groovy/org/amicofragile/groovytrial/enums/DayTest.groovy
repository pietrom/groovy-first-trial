package org.amicofragile.groovytrial.enums

import org.junit.Test;

class DayTest {
	@Test
	public void tuesdayIsWorkday() {
		def workdays = Day.MON..Day.FRI
		def tuesday = Day.TUE
		assert tuesday in workdays
	}
	
	@Test
	public void workdays() {
		def workdays = Day.MON..Day.FRI
		def log = ''
		workdays.each {current ->
			log += " $current"
		}
		assert ' MON TUE WED THU FRI' == log
	}
}
