package org.amicofragile.groovytrial.properties

import org.junit.Test

class PropertiesTest {
	@Test
	def void dotNotationIsShortcutForAccessors() {
		def book = new Book()
		book.title= 'Groovy under test';
		assert '[GROOVY UNDER TEST]' == book.title
	}
	
	@Test
	def void howToBypassAccessors() {
		def book = new Book()
		book.@title= 'Groovy under test';
		assert 'Groovy under test' == book.@title
	}
	
	@Test
	def void bypassAccessorsWhenFieldIsPrivateToo() {
		def book = new Book()
		book.@title2= 'Groovy under test';
		assert 'Groovy under test' == book.@title2
	}
}
