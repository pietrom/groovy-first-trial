package org.amicofragile.groovytrial.ranges

import org.junit.Before
import org.junit.Test

class RangesTest {
	def String accumulator

	def appendToAccumulator = { current -> accumulator += " $current" }

	@Before
	def void initAccumulator() {
		accumulator = ''
	}

	@Test
	def void extremesAndSize() {
		def theRange = 5..15
		assert 5 == theRange.from
		assert 15 == theRange.to
		assert 11 == theRange.size()
	}

	@Test
	def void inclusionTests() {
		def theRange = 5..15
		assert 11 in theRange
		assert theRange.contains(11)
		assert !(19 in theRange)
		assert !theRange.contains(19)
	}

	@Test
	def void reverse() {
		def theRange = 11..19
		assert 19..11 == theRange.reverse()
	}

	@Test
	def void dateRange() {
		def today = new Date()
		def start = today - 5
		def yesterday = today - 1
		assert yesterday in (start..today)
	}
}
