package org.amicofragile.groovytrial.ranges

import org.junit.Test

class WeekdayTest {
	@Test
	public void tuesdayIsWorkday() {
		def workdays = Weekday.MON..Weekday.FRI
		def tuesday = Weekday.TUE
		assert tuesday in workdays
	}
	
	@Test
	public void workdays() {
		def workdays = Weekday.MON..Weekday.FRI
		def log = ''
		workdays.each {current ->
			log += " $current"
		}
		assert ' Mon Tue Wed Thu Fri' == log
	}
}
