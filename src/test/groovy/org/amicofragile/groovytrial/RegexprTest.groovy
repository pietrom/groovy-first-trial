package org.amicofragile.groovytrial

import org.junit.Test

class RegexprTest {
	def String bookOnTable = "The book is on the table"
	def the = /[Tt]he/

	@Test
	def void find() {
		assert bookOnTable.find(the)
	}

	@Test
	def void match() {
		def regExpr = /[Tt].e/
		assert !(bookOnTable ==~ regExpr)
		assert bookOnTable ==~ "The .* table"
	}

	@Test
	def void findCount() {
		assert 2 == (bookOnTable =~ the).count
	}

	@Test
	def void replaceAll() {
		assert 'my book is on my table' == bookOnTable.replaceAll(the, "my")
	}

	@Test
	def void advancedReplaceAll() {
		def groupedThe = '([Tt]he)'
		def replaced = bookOnTable.replaceAll(groupedThe, /my (was $1)/)
		assert 'my (was The) book is on my (was the) table' == replaced
	}

	@Test
	def void capitalizeFirst() {
		def word = /\w+/
		def capitalized = bookOnTable.replaceAll(word) { String theWord ->
			def capFirst = { String aWord ->
				aWord[0].toUpperCase()
			}
			def rest = {String aWord ->
				aWord.length() > 1 ? aWord[1..-1] : ''
			}
			"${capFirst(theWord)}${rest(theWord)}"
		}
		assert "The Book Is On The Table" == capitalized
	}
}
