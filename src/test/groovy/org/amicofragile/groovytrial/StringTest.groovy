package org.amicofragile.groovytrial

import org.junit.Test

class StringTest {
	@Test
	def void doubleQuotesAllowTemplateInterpolation() {
		def x = "YYY"
		def template = "x's value is $x"
		assert "x's value is YYY" == template
	}
	
	@Test
	def void singleQuotesDontAllowTemplateInterpolation() {
		def x = "YYY"
		def template = 'x\'s value is $x'
		assert "x's value is \$x" == template
	}
}
