package org.amicofragile.groovytrial.mocking

import org.junit.Test

class MockStaticMethodTest {
	@Test
	def void mocksStaticJavaMethod() {
		def toTest = new ClassToTest()
		StaticProvider.metaClass.'static'.getValue = { ->
			6
		}
		assert 200 == toTest.getDoubledStaticVal() // Java code calling Java static methods see old, not-mocked version od the method itself
	}

	@Test
	def void mocksStaticGroovyMethod() {
		def toTest = new GroovyClassToTest()
		GroovyStaticProvider.metaClass.'static'.getValue = { -> 6 }
		assert 12 == toTest.getDoubledStaticValue()
	}
	
	@Test
	def void mocksStaticJavaMethodCalledFromGroovy() {
		def toTest = new GroovyClassToTest()
		GroovyStaticProvider.metaClass.'static'.getValue = { -> 6 }
		assert 12 == toTest.getDoubledJavaStaticValue()
	}

	@Test
	def void mockStaticJFCMethod() {
		// non-mocked first to prove it works normally
		String[] things = ['dog', 'ant', 'bee', 'cat']
		Arrays.sort(things)
		assert ['ant', 'bee', 'cat', 'dog'] == things

		Arrays.metaClass.'static'.sort = { String[] a ->
			a[0] = 'ewe'
			a[1] = 'fox'
		}
		
		things = ['dog', 'ant', 'bee', 'cat']
		Arrays.sort(things)
		assert ['ewe', 'fox', 'bee', 'cat'] == things
	}
}