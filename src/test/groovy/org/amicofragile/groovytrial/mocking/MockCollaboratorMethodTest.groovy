package org.amicofragile.groovytrial.mocking

import org.junit.Test

class MockCollaboratorMethodTest {
	@Test
	def void mocksCollaboratorMethodUsingMapCoercion() {
		def collaborator = [getValue : { 19 }] as Collaborator
		def toTest = new ClassToTest(collaborator)
		assert 38 == toTest.getDoubledValue()
	}
	
	@Test
	def void mocksCollaboratorMethodUsingClosureCoercion() {
		def collaborator = { 11 } as Collaborator
				def toTest = new ClassToTest(collaborator)
		assert 22 == toTest.getDoubledValue()
	}
}
