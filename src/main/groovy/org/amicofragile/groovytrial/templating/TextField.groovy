package org.amicofragile.groovytrial.templating

class TextField extends Field {
	public TextField(String name) {
		super(name);
	}
	
	def render() {
		"""<input type="text" data-lit-type="text" />"""
	}
}
