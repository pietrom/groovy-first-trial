package org.amicofragile.groovytrial.templating

class NumberField extends Field {
	public NumberField(String name) {
		super(name);
	}
	
	def render() {
		"""<input type="text" data-lit-type="number" />"""
	}
}
