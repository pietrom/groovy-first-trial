package org.amicofragile.groovytrial.templating

class Form {
	private fields
	private id
	
	Form(id) {
		this.id = id
		fields = []
	}
	
	def void addField(f) {
		fields.add(f)
	}
	
	def renderTitle() {
		id.toUpperCase()
	}
	
	def renderBody() {
		"""<form id="$id">${renderFormBody()}</form>"""
	}
	
	def renderFormBody() {
		fields.collect({field -> "\t\t\t\t" + field.render()}).join("\n")
	}
}
