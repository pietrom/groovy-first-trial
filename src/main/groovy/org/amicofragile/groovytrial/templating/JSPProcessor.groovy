package org.amicofragile.groovytrial.templating

class JSPProcessor {
	def applyTo(component) {
		"""<!-- Generated JSP Page -->
		${(heading())}
		${renderHtml(component)}
		"""
	}
	def heading() {
		"""
		<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
		"""
	}
	
	def renderHtml(component) {
		"""
			<html>
					<head>
						<title>${component.renderTitle()}</title>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					</head>
					<body>
						${component.renderBody()}
					</body>
			</html>
		"""
	}
}
