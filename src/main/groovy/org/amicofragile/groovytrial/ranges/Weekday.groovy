package org.amicofragile.groovytrial.ranges

class Weekday implements Comparable {
	static final DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
	private int index

	def static final SUN = new Weekday('Sun')
	def static final MON = new Weekday('Mon')
	def static final TUE = new Weekday('Tue')
	def static final WED = new Weekday('Wed')
	def static final THU = new Weekday('Thu')
	def static final FRI = new Weekday('Fri')
	def static final SAT = new Weekday('Sat')
	
	private Weekday(String day) {
		index = DAYS.indexOf(day)
	}
	Weekday next(){
		return new Weekday(DAYS[(index+1) % DAYS.size()])
	}
	Weekday previous(){
		return new Weekday(DAYS[index-1])
	}
	int compareTo(Object other){
		return this.index <=> other.index
	}
	String toString(){
		return DAYS[index]
	}
}
