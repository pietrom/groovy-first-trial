package org.amicofragile.groovytrial.mocking

class GroovyClassToTest {
	def getDoubledStaticValue() {
		2 * GroovyStaticProvider.getValue()
	}
	
	def getDoubledJavaStaticValue () {
		2 * StaticProvider.getValue()
	}
}
