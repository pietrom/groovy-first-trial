package org.amicofragile.groovytrial.operators

class Complex {
	def BigDecimal real
	def BigDecimal  imaginary

	def Complex(BigDecimal re, BigDecimal im) {
		real = re
		imaginary = im
	}
	
	def Complex plus(Complex that) {
		new Complex(this.real + that.real, this.imaginary + that.imaginary)
	}
	
	def Complex multiply(Number n) {
		new Complex(this.real * n, this.imaginary * n)
	}
	
	def boolean equals(Complex that) {
		this.real == that.real && this.imaginary == that.imaginary
	}
}
