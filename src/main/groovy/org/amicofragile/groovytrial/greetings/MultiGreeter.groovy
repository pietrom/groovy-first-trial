package org.amicofragile.groovytrial.greetings

class MultiGreeter {
	def Greeter greeter = new Greeter()
	
	def greet(to) {
		def greetings = to.collect {greeter.greet(it)}
		return greetings
	}
}
