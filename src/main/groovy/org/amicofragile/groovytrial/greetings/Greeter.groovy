package org.amicofragile.groovytrial.greetings

class Greeter {
	def base
	
	def Greeter(base) {
		this.base = base
	}
	
	def Greeter() {
		this("Hello")
	}
	
	def greet () {
		greet('World')
	}

	def greet (String to) {
		"$base, $to!"
	}
}
