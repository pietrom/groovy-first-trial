package org.amicofragile.groovytrial.properties

class Book {
	def String title
	private String title2
	
	def setTitle(String t) {
		title = t.toUpperCase()
	}
	
	def String getTitle() {
		"[$title]"
	}
}
