package org.amicofragile.groovytrial.mocking;

public class ClassToTest {
	private final Collaborator collaborator;

	public ClassToTest(Collaborator collaborator) {
		this.collaborator = collaborator;
	}
	
	public int getDoubledValue() {
		return 2 * collaborator.getValue();
	}
	
	public int getDoubledStaticVal() {
		return 2 * StaticProvider.getValue();
	}
}
