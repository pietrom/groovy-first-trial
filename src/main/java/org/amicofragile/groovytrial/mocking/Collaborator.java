package org.amicofragile.groovytrial.mocking;

public interface Collaborator {
	public abstract int getValue();
}
