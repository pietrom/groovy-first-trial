package org.amicofragile.groovytrial;

public class MyMath {
	public int sumTo(int n) {
		return n * (n + 1) / 2;
	}
}
